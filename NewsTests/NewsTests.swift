import XCTest
@testable import News

class NewsTests: XCTestCase {
    
    var apiClient: NewsClient!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        apiClient = Environment.apiClient as? NewsClient
    }
    
    override func tearDownWithError() throws {
        apiClient = nil
        try super.tearDownWithError()
    }
    
    func testFetchingTopHeadlines() throws {
        try XCTSkipUnless(
            Reachability.isConnectedToNetwork(), "Network connectivity needed for this test."
        )
        let promise = expectation(description: "Status code: 200")
        apiClient.fetchTopHeadlines() { (result) in
            switch result {
            case .success(_):
                promise.fulfill()
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        wait(for: [promise], timeout: 5)
    }
    
    func testFetchingLikes() throws {
        try XCTSkipUnless(
            Reachability.isConnectedToNetwork(), "Network connectivity needed for this test."
        )
        let promise = expectation(description: "Status code: 200")
        let articleID = "www.theverge.com-2020-7-21-21332300-nikon-z5-full-frame-mirrorless-camera-price-release-date-specs-index.html" // change articleID to test fetching likes
        apiClient.fetchLikesFor(articleID: articleID) { (result) in
            switch result {
            case .success(_):
                promise.fulfill()
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        wait(for: [promise], timeout: 5)
    }
    
    func testFetchingComments() throws {
        try XCTSkipUnless(
            Reachability.isConnectedToNetwork(), "Network connectivity needed for this test."
        )
        let promise = expectation(description: "Status code: 200")
        let articleID = "www.theverge.com-2020-7-21-21332300-nikon-z5-full-frame-mirrorless-camera-price-release-date-specs-index.html" // change articleID to test fetching comments
        apiClient.fetchCommentsFor(articleID: articleID) { (result) in
            switch result {
            case .success(_):
                promise.fulfill()
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        wait(for: [promise], timeout: 5)
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}

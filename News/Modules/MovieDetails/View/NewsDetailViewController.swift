import UIKit
import RxSwift

class NewsDetailViewController: UIViewController {
    
    var viewModel: NewsDetailViewModel?
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsDescriptionLabel: UILabel!
    @IBOutlet weak var newsContentLabel: UILabel!
    @IBOutlet weak var publishedOnLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        showActivityIndicator()
        setupNewsDetailsObserver()
        setupDefaultView()
    }
    
    private func setupDefaultView(){
        self.newsImageView.layer.cornerRadius = 5
        self.newsImageView.clipsToBounds = true
    }
    
    private func setupViewWith(_ article: Article){
        self.newsTitleLabel.text = article.title
        self.newsDescriptionLabel.text = article.articleDescription
        self.newsImageView.setImageWith(path: article.urlToImage ?? "")
        self.newsContentLabel.text = article.content
        self.publishedOnLabel.text = article.publishedAt.getDate()
        self.commentsLabel.text = String(describing:article.comments == nil ? 0 : article.comments!.comments)
        self.likesLabel.text = String(describing:article.likes == nil ? 0 : article.likes!.likes)
        self.authorLabel.text = article.author == nil ? article.source.name : article.author
    }

}

//MARK: RxSwift functions
extension NewsDetailViewController {
    
    func setupNewsDetailsObserver() {
        if let viewModel = viewModel {
            
            viewModel.articleData.asObservable().subscribe(onNext: { [weak self] article in
                DispatchQueue.main.async {
                    if let strongSelf = self, let article = article {
                        strongSelf.hideActivityIndicator()
                        strongSelf.setupViewWith(article)
                    }
                }
            }).disposed(by: disposeBag)
            
            // If error occurs show
            viewModel.errorRelay.asObservable().subscribe(onNext: { [weak self] error in
                DispatchQueue.main.async {
                    if let strongSelf = self, let error = error {
                        strongSelf.showAlertWithMessage(error.localizedDescription)
                        strongSelf.hideActivityIndicator()
                    }
                }
            }).disposed(by: disposeBag)
        }
    }
    
}

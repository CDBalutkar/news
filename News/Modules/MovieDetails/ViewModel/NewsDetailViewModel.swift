import RxCocoa

class NewsDetailViewModel {
    
    var newsCommentsData:CommentsData?
    var error: Error?
    var newsLikesData: LikesData?
    var articleData: BehaviorRelay<Article?> = BehaviorRelay(value: nil)
    var errorRelay: BehaviorRelay<Error?> = BehaviorRelay(value: nil)
    private let apiClient:NewsClient = Environment.apiClient as! NewsClient
    private var article: Article
    
    init(article:Article) {
        self.article = article
        fetchNewsDetailData()
    }
    
    //Used DispatchGroup as needed to fetch data from 2 apis and if either of fails still it should be considered.
    func fetchNewsDetailData() {
        let articleID = getArticleID()
        let newsDispatchGroup = DispatchGroup()
        if articleID.isEmpty {
            articleData.accept(article)
        } else {
            newsDispatchGroup.enter()
            apiClient.fetchLikesFor(articleID: articleID) { [weak self] (result) in
                newsDispatchGroup.leave()
                if let strongSelf = self {
                    switch result {
                    case .success(let newsLikesData):
                        strongSelf.newsLikesData = newsLikesData
                    case .failure(let error):
                        strongSelf.error = error
                    }
                }
            }
            newsDispatchGroup.enter()
            apiClient.fetchCommentsFor(articleID: articleID) { [weak self] (result) in
                newsDispatchGroup.leave()
                if let strongSelf = self {
                    switch result {
                    case .success(let newsCommentsData):
                        strongSelf.newsCommentsData = newsCommentsData
                    case .failure(let error):
                        strongSelf.error = error
                    }
                }
            }
        }

        newsDispatchGroup.notify(queue: DispatchQueue.global(qos: .background), execute: { [weak self] in
        if let strongSelf = self {
            if let error = strongSelf.error {
                strongSelf.errorRelay.accept(error)
            } else {
                strongSelf.article.comments = strongSelf.newsCommentsData
                strongSelf.article.likes = strongSelf.newsLikesData
                strongSelf.articleData.accept(strongSelf.article)
            }
        }
        })
        
    }
    
    private func getArticleID() -> String {
        let articleURL = URL(string:article.url)
        return articleURL?.getArticleID() ?? ""
    }
    
    
}

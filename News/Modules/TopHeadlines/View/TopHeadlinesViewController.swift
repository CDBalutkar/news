import UIKit
import RxSwift

class TopHeadlinesViewController: UIViewController, Storyboardable {
    
    var viewModel: TopHeadlinesViewModel?
    private let disposeBag = DisposeBag()
    
    @IBOutlet var newsCollectionView: UICollectionView! {
        didSet {
            newsCollectionView.collectionViewLayout = createColectionViewLayout()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newsCollectionView.registerFor(cell: TopHeadlinesCollectionViewCell.self)
        self.showActivityIndicator()
        viewModel = TopHeadlinesViewModel()
        self.title = viewModel?.title
        setupTopHeadlinesObserver()
    }
    
    private func createColectionViewLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalWidth(3/4))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalWidth(3/4))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [ item ])
        let section = NSCollectionLayoutSection(group: group)
        return UICollectionViewCompositionalLayout(section: section)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let viewModel = viewModel, segue.identifier == viewModel.segueIdentifier, let selectedArticle = viewModel.getSelectedArticle() {
            let movieDetailsViewController = segue.destination as! NewsDetailViewController
            movieDetailsViewController.viewModel = NewsDetailViewModel(article:selectedArticle)
        }
    }
    
}

//MARK: RxSwift functions
extension TopHeadlinesViewController {
    
    func setupTopHeadlinesObserver() {
        if let viewModel = viewModel {
            
            viewModel.topHeadlinesData.asObservable().subscribe(onNext: { [weak self] _ in
                DispatchQueue.main.async {
                    if let strongSelf = self {
                        strongSelf.newsCollectionView.reloadData()
                        strongSelf.hideActivityIndicator()
                    }
                }
            }).disposed(by: disposeBag)
            
            // If error occurs show
            viewModel.error.asObservable().subscribe(onNext: { [weak self] error in
                DispatchQueue.main.async {
                    // Show Alert with error object
                    if let strongSelf = self, let error = error {
                        strongSelf.showAlertWithMessage(error.localizedDescription)
                        strongSelf.hideActivityIndicator()
                    }
                }
            }).disposed(by: disposeBag)
        }
    }
    
}

extension TopHeadlinesViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let viewModel = viewModel, let topHeadlinesDataValue =  viewModel.topHeadlinesData.value {
            return topHeadlinesDataValue.articles.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: TopHeadlinesCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
        if let viewModel = viewModel, let topHeadlinesDataValue =  viewModel.topHeadlinesData.value {
            cell.topHeadlinesImageView.setImageWith(path: topHeadlinesDataValue.articles[indexPath.row].urlToImage ?? "")
            cell.topHeadlinesTitleLabel.text = topHeadlinesDataValue.articles[indexPath.row].title
        }
        return cell
    }
    
    
}

extension TopHeadlinesViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let viewModel = viewModel {
            viewModel.setSelectedNewsIndex(indexPath.row)
            performSegue(withIdentifier: viewModel.segueIdentifier, sender: nil)
        }
    }
    
}





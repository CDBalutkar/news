import UIKit

class TopHeadlinesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var topHeadlinesImageView: UIImageView!
    @IBOutlet weak var topHeadlinesTitleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
    }
     
    func setupView() {
        topHeadlinesImageView.contentMode = .scaleToFill
        containerView.setShadow()
    }
    
    

}

import RxCocoa

class TopHeadlinesViewModel {
    
    let segueIdentifier = Constants.showNewsSegueIdentifier
    let title = Constants.topHeadlines
    var topHeadlinesData: BehaviorRelay<TopHeadlinesData?> = BehaviorRelay(value: nil)
    var error: BehaviorRelay<Error?> = BehaviorRelay(value: nil)
    private let apiClient:NewsClient = Environment.apiClient as! NewsClient
    private var selectedNewsIndex: Int?
    
    
    init() {
        fetchTopHeadlines()
    }
    
    
    func fetchTopHeadlines() {
            apiClient.fetchTopHeadlines() { [unowned self] (result) in
                switch result {
                case .success(let topHeadlinesData):
                    self.topHeadlinesData.accept(topHeadlinesData)
                case .failure(let error):
                    self.error.accept(error)
                }
            }
    }
    
    func setSelectedNewsIndex(_ index:Int) {
        selectedNewsIndex = index
    }
    
    func getSelectedArticle() -> Article? {
        if let selectedNewsIndex = selectedNewsIndex, let topHeadlinesData = self.topHeadlinesData.value {
            return topHeadlinesData.articles[selectedNewsIndex]
        } else {
            return nil
        }
    }
    
}

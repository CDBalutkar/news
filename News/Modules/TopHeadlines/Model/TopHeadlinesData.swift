import Foundation

// MARK: - TopHeadlinesData
struct TopHeadlinesData: Codable {
    let status: String
    let totalResults: Int
    let articles: [Article]
}

// MARK: - Article
struct Article: Codable {
    let source: Source
    let author, articleDescription: String?
    let title: String
    let url: String
    let urlToImage: String?
    let publishedAt: Date
    let content: String?
    var comments: CommentsData?
    var likes: LikesData?

    enum CodingKeys: String, CodingKey {
        case source, author, title
        case articleDescription = "description"
        case url, urlToImage, publishedAt, content
    }
}

// MARK: - Source
struct Source: Codable {
    let id: String?
    let name: String
}

struct CommentsData: Codable {
    let comments:Int
}

struct LikesData: Codable {
    let likes:Int
}

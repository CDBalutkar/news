import Foundation

enum Environment: String {
    
    // MARK: - Environments
    
    case debug
    case release

    // MARK: - Current Environment
    
    static let current: Environment = {
        // Read Value From Info.plist
        guard let value = Bundle.main.infoDictionary?["CONFIGURATION"] as? String else {
            fatalError("No Configuration Found")
        }
        
        // Create Environment
        guard let environment = Environment(rawValue: value.lowercased()) else {
            fatalError("Invalid Environment")
        }
        
        return environment
    }()
    
    private static var baseUrl: URL {
        switch current {
        case .debug, .release:
            return URL(string: "https://newsapi.org/v2/")!
        }
    }
    //Below URl doen't need any authentication and needs to be used for fetching numbers of likes and comments.
    private static var newsHerokuAppBaseURL: URL {
        switch current {
        case .debug, .release:
            return URL(string: "https://cn-news-info-api.herokuapp.com/")!
        }
    }
    
    // MARK: - News API Key
    
    private static var apiKey: String {
        switch current {
        case .debug, .release:
            return "96ccc6fd377d498792670fa70be74db3"
        }
    }
    
    // MARK: - API Client

    static var apiClient: APIClient {
        switch current {
            case .debug, .release:
                return NewsClient(apiKey: apiKey, baseUrl: baseUrl, newsHerokuAppBaseURL: newsHerokuAppBaseURL)
        }
    }
    
}

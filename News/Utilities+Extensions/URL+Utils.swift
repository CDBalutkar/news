import Foundation

extension URL{
    
    func getArticleID() -> String {
        guard let host = host else {
            return ""
        }
        let replacedPath = path.replacingOccurrences(of: "/", with: "-", options: .literal, range: nil)
        print("articleID = \(host+replacedPath)")
        return host+replacedPath
    }
    
}

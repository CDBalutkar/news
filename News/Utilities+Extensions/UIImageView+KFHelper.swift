import Kingfisher
import UIKit

extension UIImageView {
    
    func setImageWith(path:String) {
        self.kf.indicatorType = .activity
        let newsImagePath = URL(string:path)
        self.kf.setImage(with: newsImagePath)
    }
    
}

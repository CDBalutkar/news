import Foundation

public struct Constants {
    
    static let networkErrorMessage = "Internet Or Wifi not Available !! Please connect to internet via mobile network or WIFI and try again"
    static let apiKeyText = "apikey"
    static let countryText = "country"
    static let usText = "us"
    static let contentType = "Content-Type"
    static let applicationJson = "application/json"
    static let showNewsSegueIdentifier = "showNewsDetails"
    static let topHeadlines = "Top Headlines"
    static let somethingWentWrongText = "Opps !! Something Went Wrong !!"
    static let okText = "Ok"
    
}

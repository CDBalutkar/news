import Foundation
import Alamofire

internal enum NewsAPIEndpoint: Hashable {
    
    // MARK: - Cases
    
    case topHeadlines
    case likes(String)
    case comments(String)
    
    // MARK: - Properties
    var path: String {
        switch self {
        case .topHeadlines:
            return "top-headlines"
        case .likes(let id):
            return "likes/\(id)"
        case .comments(let id):
            return "comments/\(id)"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .topHeadlines, .likes, .comments:
            return .get
        }
    }
}

import Foundation

final class NewsClient: APIClient {
    

    // MARK: - Properties

    internal var apiKey: String
    
    internal var baseUrl: URL
    
    internal var newsHerokuAppBaseURL: URL

    // MARK: - Initialization

    init(apiKey: String, baseUrl: URL, newsHerokuAppBaseURL: URL) {
        // Set Properties
        self.apiKey = apiKey
        self.baseUrl = baseUrl
        self.newsHerokuAppBaseURL = newsHerokuAppBaseURL
    }

    // MARK: - Public API

    func fetchTopHeadlines(_ completion: @escaping (Result<TopHeadlinesData, Error>) -> Void) {
        // Create and Initiate Data Task
        let request = getRequest(for: .topHeadlines)
        NetworkManager.sharedInstance.makeRequest(request, completion: completion)
    }
    
    func fetchLikesFor(articleID: String, _ completion: @escaping (Result<LikesData, Error>) -> Void) {
        // Create and Initiate Data Task
        let request = getRequest(for: .likes(articleID))
        NetworkManager.sharedInstance.makeRequest(request, completion: completion)
        // Call below only when any Json is failing
        // NetworkManager.sharedInstance.makeURLSessionDataTaskRequest(request, completion: completion)
    }
    
    func fetchCommentsFor(articleID: String, _ completion: @escaping (Result<CommentsData, Error>) -> Void) {
        // Create and Initiate Data Task
        let request = getRequest(for: .comments(articleID))
        NetworkManager.sharedInstance.makeRequest(request, completion: completion)
        // Call below only when any Json is failing
        // NetworkManager.sharedInstance.makeURLSessionDataTaskRequest(request, completion: completion)
    }

    // MARK: - Helper Methods
    private func getRequest(for endpoint: NewsAPIEndpoint, with additionalQueryItems: [URLQueryItem]? = nil) -> URLRequest {
        // Create URL
        if endpoint != .topHeadlines {
            let url = newsHerokuAppBaseURL.appendingPathComponent(endpoint.path)
            // Create Request
            var request = URLRequest(url: url)
            request.httpMethod = endpoint.method.rawValue
            
            // Configure Request
            request.addValue(Constants.applicationJson, forHTTPHeaderField: Constants.contentType)
            return request
        } else {
            let url = baseUrl.appendingPathComponent(endpoint.path)
            guard var components = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
                return URLRequest(url: url)
            }
            //Query Parameters
            var queryItems: [URLQueryItem] = [URLQueryItem(name: Constants.apiKeyText, value: apiKey), URLQueryItem(name: Constants.countryText, value: Constants.usText)]
            
            if let additionalQueryItems = additionalQueryItems {
                queryItems.append(contentsOf: additionalQueryItems)
            }
            components.queryItems = queryItems
            
            // Create Request
            var request = URLRequest(url: components.url ?? url)
            request.httpMethod = endpoint.method.rawValue
            
            // Configure Request
            request.addValue(Constants.applicationJson, forHTTPHeaderField: Constants.contentType)
            return request
        }
        
    }

}
